const PADDLE_HEIGHT = 100;
const PADDLE_THICKNESS = 10;
// const WINNING_SCORE = 1;
const MULT_CONSTANT = 3;
const BALLSPEED_X = 2.5;
const BALLSPEED_Y = 1;
const PADDLE_MULTIPLIER = 1;
// const DIFFICULTY_LEVEL = prompt("Please enter difficulty level: ");


let WINNING_SCORE = prompt("Enter win score: ")
let multiplier = prompt("Enter speed: ");
let canvas;
let canvasContext;
let ballX = 50;
let ballY = 50;
let ballSpeedX = BALLSPEED_X * multiplier;
let ballSpeedY = BALLSPEED_Y * multiplier;
let player1Paddle = 250;
let player2Paddle = 250;
let paddleMultiplier = prompt("Enter AI difficulty: ");

let p1Score = 0;
let p2Score = 0;

let winTime = false;
let paused = false;
// let changeSettings = false;

window.onload = function() {
	canvas = document.querySelector("#gameCanvas");
	canvasContext = canvas.getContext("2d");

	// every 1 second after the page is loaded, it will call drawEverything
	let framesPerSecond = 30;
	setInterval(function(){
		moveEverything();
		drawEverything();
	}, 1000/framesPerSecond);


	canvas.addEventListener('mousemove', function(pointer){
		player1Paddle = pointer.clientY - (PADDLE_HEIGHT/2);
	});

	canvas.addEventListener("mousedown", function(){
		if(winTime) {
			p1Score = 0;
			p2Score = 0;
			winTime = false;
		}
	});
	document.onkeydown = function(evt) {
	    // evt = evt || window.event;
	    if (evt.keyCode == 27) {
	        togglePause();
	    }
	    else if (evt.keyCode == 13) {
	    	changeSettings();
	    }
	};

}

function changeSettings() {
	// multiplier = 1;
	ballSpeedX = BALLSPEED_X;
	ballSpeedY = BALLSPEED_Y;
	// multiplier = MULT_CONSTANT;
	// paddleMultiplier = PADDLE_MULTIPLIER;

	multiplier = Number(prompt("Please enter  a positive number for ball speed: "));
	paddleMultiplier = Number(prompt("Enter AI difficulty: "));

	if (multiplier < 0) {
		changeSettings();
	}

	if (multiplier == 0) {
		multiplier = MULT_CONSTANT;
	}

	if (paddleMultiplier == 0) {
		paddleMultiplier = PADDLE_MULTIPLIER;
	}


	ballSpeedX *= multiplier;
	ballSpeedY *= multiplier;

}

function togglePause() {
	if (!paused) {
		paused = true;
	}
	else if (paused) {
		paused = false;
	}
}

function moveEverything() {
	if(winTime == true) {
		return;
	}

	if(paused) {
		return;
	}

	// basic ai movement
	computerMovement();

	ballX = ballX + ballSpeedX;
	ballY = ballY + ballSpeedY;

	if (ballX < 0) {
		if (ballY > player1Paddle && ballY < player1Paddle + PADDLE_HEIGHT) {
			ballSpeedX = -ballSpeedX;

			let deltaY = ballY - (player1Paddle+PADDLE_HEIGHT/2);
			ballSpeedY = deltaY * 0.35;
		}
		else {
			p2Score ++;
			ballReset();
		}
	}

	if (ballX > canvas.width) {
		// ballSpeedX = -ballSpeedX;
		if (ballY > player2Paddle && ballY < player2Paddle + PADDLE_HEIGHT) {
			ballSpeedX = -ballSpeedX;

			let deltaY = ballY - (player2Paddle+PADDLE_HEIGHT/2);
			ballSpeedY = deltaY * 0.35;
		}
		else {
			p1Score ++;
			ballReset();
		}
	}

	if (ballY > canvas.height || ballY < 0) {
		ballSpeedY = -ballSpeedY;
	}
}

function helloWorld() {
	console.log("Hello World")
}

function drawEverything() {
	if (paused) {
		canvasContext.fillStyle = "red";
		canvasContext.font = "16px Monospace";
		let him = canvasContext.fillText("Press Esc to continue", 300, 500);
		return;
	}

	// black background
	colorRect(0,0,canvas.width, canvas.height, "black");

	if(winTime) {
		canvasContext.fillStyle = "white";
		if (p1Score >= WINNING_SCORE) {
			// canvasContext.fillStyle = "white";
			canvasContext.font = "30px Verdana";
			canvasContext.fillText("Left Player Won!", 315, 200);
		}
		else if (p2Score >= WINNING_SCORE) {
			// canvasContext.fillStyle = "white";
			canvasContext.font = "30px Verdana";
			canvasContext.fillText("Right Player Won!", 315, 200)
		}

		canvasContext.font = "16px Monospace";
		canvasContext.fillText("Click to continue", 345, 500);
		return;
	}

	// if (changeSettings) {

	// }

	// left player paddle
	colorRect(0,player1Paddle,PADDLE_THICKNESS,PADDLE_HEIGHT,"lightgreen");
	// right paddle player or AI
	colorRect(canvas.width-PADDLE_THICKNESS,player2Paddle,
			PADDLE_THICKNESS,PADDLE_HEIGHT,"lightgreen");
	canvasContext.fillStyle = "white";
	canvasContext.font = "16px Monospace";
	canvasContext.fillText(p1Score, 200, 100);
	canvasContext.fillText(p2Score, canvas.width-210, 100);
	// ball
	colorCircle(ballX, ballY, 10, "orange");

	for (let i=0; i<canvas.height; i+=40) {
		colorRect(canvas.width/2-1,i,2,20,"white");
	}
}

function colorCircle(centerX, centerY, radius, drawColor) {
	canvasContext.fillStyle = drawColor;
	canvasContext.beginPath();
	canvasContext.arc(centerX, centerY, radius, 0, Math.PI*2, true);
	canvasContext.fill();
}

function colorRect(leftX, topY, width, height, drawColor) {
	canvasContext.fillStyle = drawColor;
	canvasContext.fillRect(leftX, topY, width, height);
}


function ballReset() {
	if (p1Score >= WINNING_SCORE || p2Score >= WINNING_SCORE) {
		winTime = true;
	}

	ballSpeedX = -ballSpeedX;
	ballX = canvas.width/2;
	ballY = canvas.height/2;
}

function computerMovement() {
	let player2PaddleCenter = player2Paddle + (PADDLE_HEIGHT/2);
	if(player2PaddleCenter < ballY-35) {
		player2Paddle += 6 * paddleMultiplier;
	}
	else if(player2PaddleCenter > ballY + 35){
		player2Paddle -= 6 * paddleMultiplier;
	}
}
